import React, {Component} from 'react'
import {Dimensions, Image, StyleSheet, AsyncStorage} from 'react-native'
import { AppLoading } from 'expo';
import { Container, Content, Card, CardItem, Thumbnail, Text, Button, Left,Body, Form, Item, Label, Input, View} from 'native-base';
import * as Font from 'expo-font';
// import Ionicons from 'react-native-ionicons';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { APIurl} from 'react-native-dotenv';

//const API = 'http://192.168.100.47:4000/api/';
const API = `${APIurl}/api/`;
const screenHeight = Math.round(Dimensions.get('window').height);
const screenWidth = Math.round(Dimensions.get('window').width);
// var data = {username: 'Admin',password:'123456As'};
class AppRedBins extends Component{
    constructor(props) {
      super(props);
      this.state = {
        username: '',
        password: '',
        isReady: false,
      };
    }
    
    login = () => {
	//Hacemos una constante donde haremos toda la consulta de la consulta (redundante no?), fijate que despues del nombre del archivo va "?opcion=1" esto es importante ya que con esto podemos elegir que acción hará el archivo de consulta en PHP.
            fetch(`${API}user/auth/signin`, {
		//Le indico que la consulta es de forma POST
            method: 'POST',
		//Le indico que tipo de consulta va a obtener o su contenido
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
		//Le indico que el cuerpo es un JSON y quiero que lo convierta así.
            body: JSON.stringify({
              username: this.state.username,
              password: this.state.password
            })
        }).then((response) => response.json()) 
        .then((res) => { 
         // console.log(res);
              if(res.status === 'true') {
                let UserId = res.id_usuario;
                let UserToken = res.token;
                AsyncStorage.setItem('userID',UserId.toString());
                AsyncStorage.setItem('token',UserToken.toString());
                console.log("AppRedBins -> res.token", res.token)
                this.props.navigation.navigate('Profile' );
              }else{
                alert(res.message);
              }
        }).done();
    }

    async componentDidMount() {
      
      await Expo.Font.loadAsync({
        Roboto: require('native-base/Fonts/Roboto.ttf'),
        Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
        Ionicons: require('react-native-ionicons/fonts/Ionicons.ttf')
      });
      this.setState({ isReady: true });
      this._loadInitialState().done();
    }

    _loadInitialState = async () =>{
      
      var value = await AsyncStorage.getItem('token');

      if (value !== null){
        this.props.navigation.navigate('Profile');
      }
    }
  
    render() {
      if (!this.state.isReady) {
        return <AppLoading />;
      }
  
      return (
        
        <Container style={{backgroundColor:'#f0e9f5', flexDirection:'row',flexDirection:'column', justifyContent:'center',alignSelf:'center', alignItems:'center',width:screenWidth}}>
          <Card style={{shadowColor: "#6F08B4",
            shadowOffset: {
	            width: 0,
	            height: 9,
            },
            opacity:0.95,
            shadowOpacity: 0.50,
            shadowRadius: 12.35,

            elevation: 19,alignItems:'center',width:'60%',height:'90%'}}>
        <Content contentContainerStyle={styles.content}>
            <Image
            source={require('../../../../assets/icon.png')}
            resizeMode='contain'
            style={styles.image1}>
            </Image>
            <Form style={styles.form}>
              <Item floatingLabel>
                <Label > <Ionicons name="ios-person" size={20}/>     Correo</Label>
                <Input 
                    keyboardType='email-address'
                    onChangeText= {(username) => this.setState({username}) }
                />
              </Item>
              <Item floatingLabel last>
              <Label ><Ionicons name="ios-lock" size={20}/>     Contraseña</Label>
                <Input 
                    secureTextEntry={true}
                    onChangeText= { (password)=> this.setState({password}) }
                />
              </Item>
              <Button iconLeft style={styles.buttonSignin} onPress={this.login}>
                  {/*<Icon name='log-in'/>*/}
                  <Text style={styles.textCenter}>Inicia Sesion</Text>
                </Button>
            </Form>
           {/*} <CardItem>
              <Body>
                {/*<Image source={require('../../../../img/escudo-alas.jpg')} style={{height: 150, width: 150, marginHorizontal: 90}}/>
                *
              </Body>
            </CardItem>
            <CardItem>
              {/* <Right> 
               {/*} <Button style={styles.buttonlogout} transparent onPress={this.Logout}>
                    <Text>Logout</Text>
              </Button>*
                
              {/* </Right> 
              </CardItem>*/}
              {/*</Card>*/}
              
        </Content>
        </Card>
        </Container>
     
      );
    }
  }
const styles = StyleSheet.create({
  textCenter:{
    textAlign: 'center',
    width: '100%',
    fontSize:18
  },
  content:{
    flex: 1,
    justifyContent: 'center',
    
  },
  form:{
    flex:1,
    width: 500
  },
  buttonSignin: {
    width: 500,
    height: 53,
    backgroundColor: "#6F08B4",
    marginTop: 25,
    borderRadius:50,
    alignContent:'center'
  },
  image1: {
    width: '100%',
    height:'35%',
    alignSelf: "center"
  },
  buttonlogout:{
    width: 75,
    height: 75,
    marginTop:150
  }
})

export default AppRedBins