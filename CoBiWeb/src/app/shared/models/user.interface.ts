export interface User_I{
    // id?: number;
    // nombre: string;
    // cargo: string;
    // apellidos: string;
    // telefono: string;
    // password?: string;
    // email: string;
    // photoURL?: string;
    email: string;
    password?: string;
    displayName?: string;
    photoURL?: string;
    uid?: string;
    phoneNumber?: string;
}
