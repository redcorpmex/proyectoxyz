module.exports = {
    isLoggedIn (req, res, next) {
        if(req.isAuthenticated()){
            return next();
        }
        return res.status(401).json({
            status: 'false',
            message: 'Something goes wrong',
            data: {}
        });
    },

    isNotLoggedIn(req, res, next) {
        if(!req.isAuthenticated()){
            return next();
        }
        return res.status(401).json({
            status: 'false',
            message: 'Unspecified Function',
            data: {}
        });
    }
};