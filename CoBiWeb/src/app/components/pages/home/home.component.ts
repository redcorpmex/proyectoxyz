import { Observable } from 'rxjs';
import { PostService } from './../../posts/post.service';
import { Component, OnInit } from '@angular/core';
import { CoBi_I } from '../../../shared/models/cobi.interface';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public cobi: {
    id: string;
    material: string;
    peso_bin: number;
    peso_maximo: number;
    peso_actual: number;
  }
public cobi$: Observable<CoBi_I[]>;
  constructor(private postSvc:PostService) { }


  ngOnInit() {
    this.cobi$ = this.postSvc.getAllCobis();
  }

}
