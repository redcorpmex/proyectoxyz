const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const pool = require('../database');
const helpers = require('./helpers');
const jwt = require('jsonwebtoken');

passport.use('local.signin', new LocalStrategy({
usernameField: 'username',
passwordField: 'password',
passReqToCallback: true
}, async (req, username, password, done) =>{
   console.log(req.body);
   const rows = await pool.query('SELECT * FROM usuario WHERE username = ? ',[username]);
  console.log(rows);
    if (rows.length > 0){
        const user = rows[0];
        console.log(user);
        const validPassword = await helpers.matchPassword(password, user.password);
        if (validPassword) {
            console.log(user);
            done(null, user);
        } else{
            done(null, false);
        }
    } else {
        return done(null, false);
    }
}));






/*----------------------------------------------------------------------------------------------- */

passport.use('local.signup', new LocalStrategy({
    usernameField: 'username',
    passwordField: 'password',
    passReqToCallback: true
}, async (req, username, password, done) =>{
    const { nombre, apellidos, telefono, email } = req.body;
        const newUser ={
            nombre,
            username,
            apellidos,
            telefono,
            password,
            email
        };
        newUser.password= await helpers.encryptPassword(password);
        const result = await pool.query('INSERT INTO usuario SET ?',[newUser]);
        newUser.id_usuario = result.insertId;
        // console.log(newUser);
        const token = jwt.sign({id: newUser.id_usuario},process.env.PASSWORD_JWT,{
            expiresIn: 60 * 15
        });
        console.log(token);
        newUser.token = token;
        console.log(newUser);
        return done(null, newUser);
}));

passport.serializeUser((user, done) => {
    done(null, user.id_usuario);
});

passport.deserializeUser( async (id_usuario, done) =>{
  const rows = await pool.query('SELECT * FROM usuario WHERE id_usuario = ?',[id_usuario]);
//   console.log(rows);
const token = jwt.sign({id: rows[0].id},process.env.PASSWORD_JWT,{expiresIn: 60 * 15});
const user = rows[0];
user.token = token;
user.status = 'true';
   done(null, user);
});
