import '@babel/polyfill'
const NODE_ENV = process.env.NODE_ENV || 'development';
require('dotenv').config({
    path:`.env.${NODE_ENV}`
});
const express = require('express');
const { database } = require('./keys');
const morgan = require('morgan');
const session = require('express-session');
const MySQLStore = require('express-mysql-session');

const passport = require('passport');

//Inicializations
const app = express();
require('./lib/passport');
require('./controllers/webSockets');
console.log(process.env.NODE_ENV);

// Settings
app.set('port', process.env.PORT || 3000);

// middlewares
app.use(session({

    // store: new MySQLStore({
    //     pool : pool,                // Connection pool
    //     // tableName : 'session'   // Use another table-name than the default "session" one
    //   }),
      secret: process.env.PASSWORD_PASSPORT,
      resave: false,
      saveUninitialized: false,
      cookie: { maxAge: 10 * 60 * 1000 }, //10 Min
      store: new MySQLStore(database) //En caso de usar otra conexion
}));
app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(passport.initialize());
app.use(passport.session());

// routes
app.use('/api/user',require('./routes/users'));
app.use('/api/task',require('./routes/tasks'));

async function init(){
    await app.listen(app.get('port'), () =>{
        console.log('Server on Port', app.get('port'));
        });
}

init();