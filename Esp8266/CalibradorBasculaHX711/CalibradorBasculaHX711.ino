//1020 
#include "HX711.h"
 
// Pin de datos y de reloj
byte pinData = 14;
byte pinClk = 12;
 
HX711 bascula;
 
// Parámetro para calibrar el peso y el sensor
//float factor_calibracion = 20780.0; //Este valor del factor de calibración funciona para mi. El tuyo probablemente será diferente.
// MI factor: 296.17 
float factor_calibracion = 296.17;
void setup() {
  Serial.begin(115200);
  Serial.println("HX711 programa de calibracion");
  Serial.println("Quita cualquier peso de la bascula");
  //Serial.println("Una vez empiece a mostrar informacion de medidas, coloca un peso conocido encima de la bascula");
  //Serial.println("Presiona + para incrementar el factor de calibracion");
  //Serial.println("Presiona - para disminuir el factor de calibracion");
 
  // Iniciar sensor
  bascula.begin(pinData, pinClk);
 
  // Aplicar la calibración
  //bascula.set_scale(281622.59);
  bascula.set_scale();
  // Iniciar la tara
  // No tiene que haber nada sobre el peso
  bascula.tare();
  Serial.println("Pon un peso conocido");
delay(10000);
Serial.println(bascula.get_units(10),8);
  // Obtener una lectura de referencia
 
  // Mostrar la primera desviación
  
}
 
void loop() {
 
  // Aplicar calibración
   //Serial.println(bascula.get_units(10),8);
  
}
