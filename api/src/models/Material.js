const {Schema ,model} = require('mongoose');

const MaterialSchema =  new Schema({
    nombre: String,
    peso_unitario: Number,
    created: { 
        type: Date,
        default: Date.now
    }
});

module.exports = model('Material', MaterialSchema);