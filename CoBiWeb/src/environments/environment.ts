// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyBS8MYBBMG1MNN4qsrraFK-I9G-DNdKJVw",
    authDomain: "cobinet-58d35.firebaseapp.com",
    databaseURL: "https://cobinet-58d35.firebaseio.com",
    projectId: "cobinet-58d35",
    storageBucket: "cobinet-58d35.appspot.com",
    messagingSenderId: "244220887153",
    appId: "1:244220887153:web:0610cebfff4ff210ea1bd0",
    measurementId: "G-LBBP5DNHK0"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
