import { ContainerAppComponent } from './components/pages/container-app/container-app.component';
import { PostComponent } from './components/posts/post/post.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './shared/guards/auth.guard';

const routes: Routes = [

{
  path: '',
  component: ContainerAppComponent,
  children:[
    { path: 'home', loadChildren: () => import('./components/pages/home/home.module').then(m => m.HomeModule), canActivate: [AuthGuard] },

    { path: 'cobi/:id', component: PostComponent, canActivate: [AuthGuard]},

    { path: 'about', loadChildren: () => import('./components/pages/about/about.module').then(m => m.AboutModule) },

    { path: 'welcome', loadChildren: () => import('./components/pages/welcome/welcome.module').then(m => m.WelcomeModule) },

    { path: '', redirectTo: 'welcome', pathMatch: 'full' },

    { path: 'login', loadChildren: () => import('./components/auth/login/login.module').then(m => m.LoginModule) },

    { path: 'admin', loadChildren: () => import('./components/admin/admin.module').then(m => m.AdminModule), canActivate: [AuthGuard] },

    { path: 'cobis', loadChildren: ()=> import('./components/posts/list-posts/list-posts.module').then(m => m.ListPostsModule), canActivate: [AuthGuard]},

    { path: 'profile', loadChildren: () => import('./components/admin/profile/profile.module').then(m => m.ProfileModule), canActivate: [AuthGuard] }

  ],

},



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
