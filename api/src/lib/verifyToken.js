const jwt = require('jsonwebtoken');

function verifyToken (req, res, next){

    const token = req.headers['x-access-token'];
 
    if (token) {
      jwt.verify(token, process.env.PASSWORD_JWT, (err, decoded) => {      
        if (err) {
          return res.status(401).json({ 
            status: 'false',
            mensaje: 'Invalid Token'
           });    
        } else {
          req.decoded = decoded;    
          next();
        }
      });
    } else {
      res.status(401).json({ 
          status: 'false',
          mensaje: 'No Token Provided' 
      });
    }
}
module.exports = verifyToken;