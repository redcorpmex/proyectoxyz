import React from 'react'
import {Button} from 'react-native';
import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Login from './modules/login/containers/login';
import Profile from './modules/profile/container/profile';
const LoginNav = createStackNavigator({
    Login: {
        screen: Login,
        navigationOptions: {
            title: 'Inicia Sesion',
            headerStyle:{
                backgroundColor: '#673ab7',
                },
                headerTintColor: '#fff',
                headerTitleStyle: {
                    fontWeight: 'bold',
                    opacity:54,
                },
                
        }
    },
    Profile: {
        screen: Profile,
        
        navigationOptions : ({ navigation }) => {
            const { params } = navigation.state;
            return params;
        } 
    }
}
,{headerMode: 'screen', headerLayoutPreset: 'center'});

export default createAppContainer(LoginNav)
