//Ported to ESP32
#ifdef ESP32
#include <FS.h>
#include "SPIFFS.h"
#include <esp_wifi.h>
#include <WiFi.h>
#include <WiFiClient.h>


#define ESP_getChipId()   ((uint32_t)ESP.getEfuseMac())

#define LED_BUILTIN       16
#define LED_ON            HIGH
#define LED_OFF           LOW

#else
#include <FS.h>                   //this needs to be first, or it all crashes and burns...

#include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino
//needed for library
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <ElegantOTA.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <ESP8266HTTPClient.h>
#include "HX711.h"
//#include <SocketIoClient.h>



#include <Arduino.h>

#include <ESP8266WiFiMulti.h>


#include <WebSocketsClient.h>
#include <SocketIOclient.h>

#include <Hash.h>


//String ip = "192.168.100.47";                 //name: blynk_server
int port = 4001;                                //Important
String url = "/api/task/bins/data/update/";     //Important
//String id = "5e326738962c4832e04c4ddf";       //name: blynk_port
//String Token = "My_Token_CoBi";               //name: blynk_token


HTTPClient http;

#define DEBUG_HX711
#define CALIBRACION 281277.91729
#define ESP_getChipId()   (ESP.getChipId())

#define LED_ON      LOW
#define LED_OFF     HIGH
#endif

// Pin D2 mapped to pin GPIO2/ADC12 of ESP32, or GPIO2/TXD1 of NodeMCU control on-board LED
#define PIN_LED       LED_BUILTIN

#include <ESP_WiFiManager.h>              //https://github.com/khoih-prog/ESP_WiFiManager

// Now support ArduinoJson 6.0.0+ ( tested with v6.14.1 )
#include <ArduinoJson.h>          //https://github.com/bblanchon/ArduinoJson

char configFileName[] = "/config.json";

// SSID and PW for your Router
String Router_SSID;
String Router_Pass;

// SSID and PW for Config Portal
String AP_SSID;
String AP_PASS;

//clock & data hx711
byte pinData = 14;
byte pinClk = 12;



//define your default values here, if there are different values in configFileName (config.json), they are overwritten.
#define BLYNK_SERVER_LEN                64
#define BLYNK_TOKEN_LEN                 32
#define BLYNK_SERVER_PORT_LEN           32
#define MQTT_SERVER_MAX_LEN             40
#define MQTT_SERVER_PORT_LEN            32

char blynk_server [BLYNK_SERVER_LEN]        = "192.168.100.47";
char blynk_port   [BLYNK_SERVER_PORT_LEN]   = "5e326738962c4832e04c4ddf";
char blynk_token  [BLYNK_TOKEN_LEN]         = "5e509f772c6f7c07b38b7abc";

char mqtt_server  [MQTT_SERVER_MAX_LEN]     = "Panel#3";
char mqtt_port    [MQTT_SERVER_PORT_LEN]    = "5e509fdc2c6f7c07b38b7abe";


ESP8266WebServer server(80);

SocketIOclient socketIO;

HX711 bascula;

//flag for saving data
bool shouldSaveConfig = false;

//callback notifying us of the need to save config
void saveConfigCallback(void)
{
  Serial.println("Should save config");
  shouldSaveConfig = true;
}

bool loadSPIFFSConfigFile(void)
{
  //clean FS, for testing
  //SPIFFS.format();

  //read configuration from FS json
  Serial.println("Mounting FS...");

  if (SPIFFS.begin())
  {
    Serial.println("Mounted file system");

    if (SPIFFS.exists(configFileName))
    {
      //file exists, reading and loading
      Serial.println("Reading config file");
      File configFile = SPIFFS.open(configFileName, "r");

      if (configFile)
      {
        Serial.print("Opened config file, size = ");
        size_t configFileSize = configFile.size();
        Serial.println(configFileSize);

        // Allocate a buffer to store contents of the file.
        std::unique_ptr<char[]> buf(new char[configFileSize + 1]);

        configFile.readBytes(buf.get(), configFileSize);

        Serial.print("\nJSON parseObject() result : ");

#if (ARDUINOJSON_VERSION_MAJOR >= 6)
        DynamicJsonDocument json(1024);
        auto deserializeError = deserializeJson(json, buf.get(), configFileSize);

        if ( deserializeError )
        {
          Serial.println("failed");
          return false;
        }
        else
        {
          Serial.println("OK");

          if (json["blynk_server"])
            strncpy(blynk_server, json["blynk_server"], sizeof(blynk_server));
         
          if (json["blynk_port"])
            strncpy(blynk_port, json["blynk_port"], sizeof(blynk_port));
 
          if (json["blynk_token"])
            strncpy(blynk_token,  json["blynk_token"], sizeof(blynk_token));

          if (json["mqtt_server"])
            strncpy(mqtt_server, json["mqtt_server"], sizeof(mqtt_server));

          if (json["mqtt_port"])  
            strncpy(mqtt_port,   json["mqtt_port"], sizeof(mqtt_port));
        }

        //serializeJson(json, Serial);
        serializeJsonPretty(json, Serial);
#else
        DynamicJsonBuffer jsonBuffer;
        // Parse JSON string
        JsonObject& json = jsonBuffer.parseObject(buf.get());
        // Test if parsing succeeds.

        if (json.success())
        {
          Serial.println("OK");

          if (json["blynk_server"])
            strncpy(blynk_server, json["blynk_server"], sizeof(blynk_server));
         
          if (json["blynk_port"])
            strncpy(blynk_port, json["blynk_port"], sizeof(blynk_port));
 
          if (json["blynk_token"])
            strncpy(blynk_token,  json["blynk_token"], sizeof(blynk_token));

          if (json["mqtt_server"])
            strncpy(mqtt_server, json["mqtt_server"], sizeof(mqtt_server));

          if (json["mqtt_port"])  
            strncpy(mqtt_port,   json["mqtt_port"], sizeof(mqtt_port));
        }
        else
        {
          Serial.println("failed");
          return false;
        }
        //json.printTo(Serial);
        json.prettyPrintTo(Serial);
#endif

        configFile.close();
      }
    }
  }
  else
  {
    Serial.println("failed to mount FS");
    return false;
  }
  return true;
}

void socketIOEvent(socketIOmessageType_t type, uint8_t * payload, size_t length) {
    switch(type) {
        case sIOtype_DISCONNECT:
            Serial.printf("[IOc] Disconnected!\n");
            break;
        case sIOtype_CONNECT:
            Serial.printf("[IOc] Connected to url: %s\n", payload);
            break;
        case sIOtype_EVENT:
            Serial.printf("[IOc] get event: %s\n", payload);
            break;
        case sIOtype_ACK:
            Serial.printf("[IOc] get ack: %u\n", length);
            hexdump(payload, length);
            break;
        case sIOtype_ERROR:
            Serial.printf("[IOc] get error: %u\n", length);
            hexdump(payload, length);
            break;
        case sIOtype_BINARY_EVENT:
            Serial.printf("[IOc] get binary: %u\n", length);
            hexdump(payload, length);
            break;
        case sIOtype_BINARY_ACK:
            Serial.printf("[IOc] get binary ack: %u\n", length);
            hexdump(payload, length);
            break;
    }
}

bool saveSPIFFSConfigFile(void)
{
  Serial.println("Saving config");

#if (ARDUINOJSON_VERSION_MAJOR >= 6)
  DynamicJsonDocument json(1024);
#else
  DynamicJsonBuffer jsonBuffer;
  JsonObject& json = jsonBuffer.createObject();
#endif

  json["blynk_server"] = blynk_server;
  json["blynk_port"]   = blynk_port;
  json["blynk_token"]  = blynk_token;

  json["mqtt_server"] = mqtt_server;
  json["mqtt_port"]   = mqtt_port;

  File configFile = SPIFFS.open(configFileName, "w");

  if (!configFile)
  {
    Serial.println("Failed to open config file for writing");
  }

#if (ARDUINOJSON_VERSION_MAJOR >= 6)
  //serializeJson(json, Serial);
  serializeJsonPretty(json, Serial);
  // Write data to file and close it
  serializeJson(json, configFile);
#else
  //json.printTo(Serial);
  json.prettyPrintTo(Serial);
  // Write data to file and close it
  json.printTo(configFile);
#endif

  configFile.close();
  //end save
}

void heartBeatPrint(void)
{
  static int num = 1;

  if (WiFi.status() == WL_CONNECTED)
    Serial.print("H");        // H means connected to WiFi
  else
    Serial.print("F");        // F means not connected to WiFi

  if (num == 80)
  {
    Serial.println();
    num = 1;
  }
  else if (num++ % 10 == 0)
  {
    Serial.print(" ");
  }
}

void toggleLED()
{
  //toggle state
  digitalWrite(PIN_LED, !digitalRead(PIN_LED));
}
unsigned long messageTimestamp = 0;
void SocketIoEnv(){
  socketIO.loop();
  //delay(2000);
 
//  #ifdef DEBUG_HX711
//  Serial.print("[HX7] Leyendo: ");
//  //Serial.print(bascula.get_units(), 1);
//  Serial.print(" Kg");
//  Serial.println();
//  #endif

  


  

 
//  if (WiFi.status() == WL_CONNECTED) {
// 
//    webSocket.emit("ESP",("\""+String(p)+"\"").c_str());
//    
//  }

  uint64_t now = millis();

  
  if(now - messageTimestamp > 2000) {
        messageTimestamp = now;

        float p = bascula.get_units();

              // Check if any reads failed and exit early (to try again).
        if (isnan(p)) {
          Serial.println(F("Failed to read from HX711!"));
          return;
        }
        // creat JSON message for Socket.IO (event)
        DynamicJsonDocument doc1(1024);
        DynamicJsonDocument doc2(1024);
        DynamicJsonDocument doc3(1024);
        //DynamicJsonDocument doc4(1024);

        
        JsonArray array1 = doc1.to<JsonArray>();
        JsonArray array2 = doc2.to<JsonArray>();
        JsonArray array3 = doc3.to<JsonArray>();
        //JsonArray array4 = doc4.to<JsonArray>();
        
        // add evnet name
        // Hint: socket.on('event_name', ....
        array1.add("ESP");
        array2.add("ESP");
        array3.add("ESP");
        //array4.add("ESP");

        // add payload (parameters) for the event
        JsonObject param1 = array1.createNestedObject();
        JsonObject param2 = array2.createNestedObject();
        JsonObject param3 = array3.createNestedObject();
        //JsonObject param4 = array4.createNestedObject();
        
        param1["id"] = blynk_port;
        param1["ubicacion"] = mqtt_server;
        param1["peso_actual"] = p;

        param2["id"] = blynk_token;
        param2["ubicacion"] = mqtt_server;
        param2["peso_actual"] = 4.5;

        param3["id"] = mqtt_port;
        param3["ubicacion"] = mqtt_server;
        param3["peso_actual"] = 7.9;

        //param4["id"] = mqtt_port;
        //param4["ubicacion"] = mqtt_server;
        //param4["peso_actual"] = random(1,10);

        // JSON to String (serializion)
        String output1;
        String output2;
        String output3;
        //String output4;
        
        serializeJson(doc1, output1);
        serializeJson(doc2, output2);
        serializeJson(doc3, output3);
        //serializeJson(doc4, output4);

        // Send event        
        socketIO.sendEVENT(output1);
        socketIO.sendEVENT(output2);
        socketIO.sendEVENT(output3);
        //socketIO.sendEVENT(output4);

        // Print JSON for debugging
        Serial.println(output1);
        Serial.println(output2);
        Serial.println(output3);
        //Serial.println(output4);
    }
  
  }


void check_status()
{
  static ulong checkstatus_timeout  = 0;
  static ulong LEDstatus_timeout    = 0;
  static ulong currentMillis;

#define HEARTBEAT_INTERVAL    10000L
#define LED_INTERVAL          2000L

  currentMillis = millis();

  if ((currentMillis > LEDstatus_timeout) || (LEDstatus_timeout == 0))
  {
    // Toggle LED at LED_INTERVAL = 2s
    toggleLED();
    LEDstatus_timeout = currentMillis + LED_INTERVAL;
  }

  // Print hearbeat every HEARTBEAT_INTERVAL (10) seconds.
  if ((currentMillis > checkstatus_timeout) || (checkstatus_timeout == 0))
  {
    heartBeatPrint();
    checkstatus_timeout = currentMillis + HEARTBEAT_INTERVAL;
  }
}


void socket_Connected(const char * payload, size_t length) {
  Serial.print("Socket on");
  
}

void setup()
{
///////////////////////////////////////////////////////
// Setup Bascula
  #ifdef DEBUG_HX711
  Serial.begin(115200);
  Serial.println("[HX7] Inicio del sensor HX711");
  #endif
 
  // Iniciar sensor
  bascula.begin(pinData, pinClk);
  // Aplicar la calibración
  bascula.set_scale(CALIBRACION);
  // Iniciar la tara
  // No tiene que haber nada sobre el peso
  bascula.tare();
/////////////////////////////////////////////////////////
  
  Serial.println("\nStarting AutoConnectWithFSParams");
  
  WiFi.mode(WIFI_STA);
  
  loadSPIFFSConfigFile();

  // The extra parameters to be configured (can be either global or just in the setup)
  // After connecting, parameter.getValue() will get you the configured value
  // id/name placeholder/prompt default length
  ESP_WMParameter custom_blynk_server("blynk_server", "Server", blynk_server, BLYNK_SERVER_LEN + 1);
  ESP_WMParameter custom_blynk_port  ("blynk_port",   "ID",   blynk_port,   BLYNK_SERVER_PORT_LEN + 1);
  ESP_WMParameter custom_blynk_token ("blynk_token",  "ID2",  blynk_token,  BLYNK_TOKEN_LEN + 1 );
  ESP_WMParameter custom_mqtt_port  ("mqtt_port",   "ID3",   mqtt_port,   MQTT_SERVER_PORT_LEN + 1);
  
  ESP_WMParameter custom_mqtt_server("mqtt_server", "Ubicacion", mqtt_server, MQTT_SERVER_MAX_LEN + 1);

  // Use this to default DHCP hostname to ESP8266-XXXXXX or ESP32-XXXXXX
  ESP_WiFiManager ESP_wifiManager;
  // Use this to personalize DHCP hostname (RFC952 conformed)
  //ESP_WiFiManager ESP_wifiManager("AutoConnect-FSParams");

  //set config save notify callback
  ESP_wifiManager.setSaveConfigCallback(saveConfigCallback);

  //add all your parameters here
  ESP_wifiManager.addParameter(&custom_blynk_server);
  ESP_wifiManager.addParameter(&custom_blynk_port);
  ESP_wifiManager.addParameter(&custom_blynk_token);

  ESP_wifiManager.addParameter(&custom_mqtt_server);
  ESP_wifiManager.addParameter(&custom_mqtt_port);

  //reset settings - for testing
  //ESP_wifiManager.resetSettings();

  ESP_wifiManager.setDebugOutput(true);

  //set minimu quality of signal so it ignores AP's under that quality
  //defaults to 8%
  //ESP_wifiManager.setMinimumSignalQuality();

  //set custom ip for portal
  ESP_wifiManager.setAPStaticIPConfig(IPAddress(192, 168, 100, 1), IPAddress(192, 168, 100, 1), IPAddress(255, 255, 255, 0));

  ESP_wifiManager.setMinimumSignalQuality(-1);
  // Set static IP, Gateway, Subnetmask, DNS1 and DNS2. New in v1.0.5+
  //ESP_wifiManager.setSTAStaticIPConfig(IPAddress(192, 168, 2, 114), IPAddress(192, 168, 2, 1), IPAddress(255, 255, 255, 0),
  //                                     IPAddress(192, 168, 2, 1), IPAddress(8, 8, 8, 8));

  // We can't use WiFi.SSID() in ESP32 as it's only valid after connected.
  // SSID and Password stored in ESP32 wifi_ap_record_t and wifi_config_t are also cleared in reboot
  // Have to create a new function to store in EEPROM/SPIFFS for this purpose
  Router_SSID = ESP_wifiManager.WiFi_SSID();
  Router_Pass = ESP_wifiManager.WiFi_Pass();

  //Remove this line if you do not want to see WiFi password printed
  Serial.println("\nStored: SSID = " + Router_SSID + ", Pass = " + Router_Pass);

  if (Router_SSID != "")
  {
    ESP_wifiManager.setConfigPortalTimeout(60); //If no access point name has been previously entered disable timeout.
    Serial.println("Got stored Credentials. Timeout 60s");
  }
  else
  {
    Serial.println("No stored Credentials. No timeout");
  }

  String chipID = String(ESP_getChipId(), HEX);
  chipID.toUpperCase();

  // SSID and PW for Config Portal
  AP_SSID = "CoBi" + chipID + "_AP";
  AP_PASS = "MyCoBi_" + chipID;

  // Get Router SSID and PASS from EEPROM, then open Config portal AP named "ESP_XXXXXX_AutoConnectAP" and PW "MyESP_XXXXXX"
  // 1) If got stored Credentials, Config portal timeout is 60s
  // 2) If no stored Credentials, stay in Config portal until get WiFi Credentials
  if (!ESP_wifiManager.autoConnect(AP_SSID.c_str(), AP_PASS.c_str()))
  {
    Serial.println("failed to connect and hit timeout");

    //reset and try again, or maybe put it to deep sleep
#ifdef ESP8266
    ESP.reset();
#else   //ESP32
    ESP.restart();
#endif
    delay(1000);
  }

  //if you get here you have connected to the WiFi
  Serial.println("WiFi connected");

  //read updated parameters
  strncpy(blynk_server, custom_blynk_server.getValue(), sizeof(blynk_server));
  strncpy(blynk_port,   custom_blynk_port.getValue(),   sizeof(blynk_port));
  strncpy(blynk_token,  custom_blynk_token.getValue(),  sizeof(blynk_token));

  strncpy(mqtt_server, custom_mqtt_server.getValue(), sizeof(mqtt_server));
  strncpy(mqtt_port, custom_mqtt_port.getValue(),     sizeof(mqtt_port));

  //save the custom parameters to FS
  if (shouldSaveConfig)
  {
    saveSPIFFSConfigFile();
  }

  Serial.println("local ip");
  Serial.println(WiFi.localIP());

  
  server.on("/", []() {
    server.send(200, "text/plain", "Hi! I am CoBi.");
  });
  

  ElegantOTA.begin(&server);    // Start ElegantOTA
  server.begin();
  Serial.println("HTTP server started");

   ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH) {
      type = "sketch";
    } else { // U_FS
      type = "filesystem";
    }

    // NOTE: if updating FS this would be the place to unmount FS using FS.end()
    Serial.println("Start updating " + type);
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) {
      Serial.println("Auth Failed");
    } else if (error == OTA_BEGIN_ERROR) {
      Serial.println("Begin Failed");
    } else if (error == OTA_CONNECT_ERROR) {
      Serial.println("Connect Failed");
    } else if (error == OTA_RECEIVE_ERROR) {
      Serial.println("Receive Failed");
    } else if (error == OTA_END_ERROR) {
      Serial.println("End Failed");
    }
  });
  ArduinoOTA.begin();
  Serial.println("Ready");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  
  
 // socketIO.on("connect", socket_Connected);
  socketIO.begin(blynk_server, port);
  socketIO.onEvent(socketIOEvent);
  
}

void loop()
{
  SocketIoEnv();
  check_status();
  server.handleClient();
  ArduinoOTA.handle();
}
