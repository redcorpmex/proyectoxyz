const app = require('express')();
const server = require('http').createServer(app);
const io = require('socket.io').listen(server);
//const {updateData} = require('./tasks.controller')
const http = require('http');
let prevUbi = "";
let prevID = "";
let Ubicaciones = [
  /*{
    ubicacion : String,
    IDs : [],
    pesos: []
  }*/
]

let newID;
let ubiIndex;
let IDindex;
let myJSON;
let otherJSON;
let newClient;
let newUbi;
let UbiString;
let UbiJson

const NODE_ENV = process.env.NODE_ENV || 'development';
require('dotenv').config({
    path:`.env.${NODE_ENV}`
});
    io.on('connect', socket => {//Ejecutado cuando se conecta un cliente. Mandar id aquí
        newClient = true;
        console.log('Client connected');
        console.log('ID : '+socket.id);
        socket.on('ESP', data => {
          
          //console.log('ID : '+socket.id);
          newID = true;
          newUbi = true;
          if(prevUbi !== data.ubicacion){//
          // console.log("TCL: different prevUbi", prevUbi)
              
            if(Ubicaciones.length>0){
            for(let [indice,element] of Ubicaciones.entries()){
              // console.log("TCL: element.ubicacion", element.ubicacion)
              if(element.ubicacion == data.ubicacion ){
                  prevUbi = data.ubicacion;
                  newUbi = false;
                  ubiIndex = indice;//////////////
                  break;
              }
            }
          }
          else{
            ubiIndex = 0;
          }
          }
          else{
            // console.log("TCL: same prevUbi", prevUbi)
            newUbi = false; 
          }
          if(newUbi){
            let dataFromNewUbi = {
              ubicacion : data.ubicacion,
              IDs : [data.id],
              pesos: [data.peso_actual],
              material_peso_maximo: [],
              material_peso_unitario : [],
              material_nombre : [],
              bajo_10:[],
              alertEnviado:[]
            }
            Ubicaciones.push(dataFromNewUbi)
            UbiString = JSON.stringify({
              Ubicaciones: Ubicaciones 
            });
            UbiJson = JSON.parse(UbiString);
            console.log("ENV FOR NEW UBI");
            io.emit("newUbi",UbiJson);
            ubiIndex = Ubicaciones.length-1;
            // // console.log("TCL: ubiIndex", ubiIndex);
            // // console.log("TCL: Ubicaciones", Ubicaciones);
            prevUbi=data.ubicacion;
          }
          else {
            if(prevID !== data.id){
              if(Ubicaciones[ubiIndex].IDs.length>0){ // parece no ser necesario
              for(let [indice,element] of Ubicaciones[ubiIndex].IDs.entries()){
                if(element == data.id ){
                    prevID = data.id;
                    newID = false;
                    IDindex = indice;
                    break;
                }
              }
            }
            else{
              IDindex = 0;
            }
            }
            else{
              newID = false; 
            }
            if(newID){
              Ubicaciones[ubiIndex].ubicacion = data.ubicacion;
              Ubicaciones[ubiIndex].IDs.push(data.id);
              Ubicaciones[ubiIndex].pesos.push(data.peso_actual);
              UbiString = JSON.stringify({
                ubicacion : Ubicaciones[ubiIndex].ubicacion,
                posicion: ubiIndex,
                IDs : Ubicaciones[ubiIndex].IDs,
                pesos: Ubicaciones[ubiIndex].pesos 
              });
              UbiJson = JSON.parse(UbiString);
              console.log("ENV FOR NEW ID");
              if(!newClient){
                io.emit("newID",UbiJson);
              }
              
              IDindex = Ubicaciones[ubiIndex].IDs.length-1;// Si vuelve a entrar el mismo Id que acaba de entrar, no se asignaría el id Index en ningún lado, por lo que se asigna desde la primera vez que entra.
              // // console.log("TCL: IDindex", IDindex);
              // // console.log("TCL: Ubicaciones", Ubicaciones)
              prevID=data.id;
            }
            else{
              Ubicaciones[ubiIndex].pesos[IDindex]= data.peso_actual;
              console.log("ENV PESO");
              // // console.log("TCL: ubiIndex", ubiIndex)
              // // console.log("TCL: IDindex", IDindex)
              UbiString = JSON.stringify({
                posicion: ubiIndex,
                pesos: Ubicaciones[ubiIndex].pesos 
              });
              UbiJson = JSON.parse(UbiString);
              if(!newClient){
                io.emit("Peso",UbiJson);
              }
              
              // // console.log("TCL: Ubicaciones", Ubicaciones)
            }
          }
          
          
            //io.emit("Peso",data);
            
            if( newClient){
              console.log("ENV FOR  NEW CLIENT")
              let NewClientString  = JSON.stringify({
              Ubicaciones: Ubicaciones
            });
            let NewClientJSON = JSON.parse(NewClientString);
              io.emit("newUbi",NewClientJSON);
              // // console.log("TCL: Ubicaciones", Ubicaciones)
              newClient = false;
            }
            console.log('fromESP: ',data);     
        });

        socket.on('disconnect', (reason) => {
        if (reason === 'io server disconnect') {
          // the disconnection was initiated by the server, you need to reconnect manually
          socket.connect();
        }
        // else the socket will automatically try to reconnect
      });
      
      });

server.listen(4001,() => { console.log('WSServer running on port 4001') });

setInterval(()=>{dataBaseUpdate(Ubicaciones)},10000);



function dataBaseUpdate(obj){
  for(let [indice,elemento] of obj.entries()){
    for(let [index,element] of elemento.IDs.entries()){
      const postData = JSON.stringify({
        peso_actual: Ubicaciones[indice].pesos[index]
      });
    
    //console.log(intDato)
    const options = {
    hostname: process.env.HOST_DB,
    port: 4000,
    path: `/api/task/bins/data/update/${element}`,
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Content-Length': Buffer.byteLength(postData)
    }
    //body :  {"peso_actual": intDato}
    };
    
    const req = http.request(options, (res) => {
    //console.log(`STATUS: ${res.statusCode}`);
    //console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
    res.setEncoding('utf8');
    res.on('data', (chunk) => {
      console.log(`BODY: ${chunk}`);
    });
    res.on('end', () => {
      console.log('No more data in response.');
    });
    });
    
    req.on('error', (e) => {
    console.error(`problem with request: ${e.message}`);
    });
    
    // Write data to request body
    req.write(postData);
    req.end();
    }
    
    
  }
  //clearInterval(myTimer);
}
