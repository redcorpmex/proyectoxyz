import { Router } from '@angular/router';
import { File_I } from './../../../shared/models/file.interface';
import { User_I } from './../../../shared/models/user.interface';
import { AuthService } from './../../../shared/service/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  public image: File_I;
  public currentImage = 'https://picsum.photos/id/113/150/150';
  constructor(private authSvc: AuthService, private router: Router) { }

  public profileForm = new FormGroup({
    displayName: new FormControl('', Validators.required),
    email: new FormControl({value: '', disabled: true}, Validators.required),
    photoURL: new FormControl('', Validators.required),
    uid: new FormControl('', Validators.required),
    phoneNumber: new FormControl('', Validators.required)

  });

  ngOnInit() {
    this.authSvc.userData$.subscribe(user => {
      this.initValuesForm(user);
    })
  }

  onSaveUser(user: User_I): void{
    this.authSvc.preSaveUserProfile(user, this.image);
  }

  private initValuesForm(user: User_I): void{
    if (user.photoURL) {
      this.currentImage = user.photoURL;
    }
    this.profileForm.patchValue({
      displayName: user.displayName,
      email: user.email,
      uid: user.uid,
      phoneNumber: user.phoneNumber
    })
  }

  handleImage(image: File_I): void {
    this.image = image;
  }

}
