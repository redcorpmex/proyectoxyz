import { PostService } from './../post.service';
import { CoBi_I } from './../../../shared/models/cobi.interface';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';


@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.scss']
})
export class NewPostComponent implements OnInit {
  private image: any

  constructor(private postSvc: PostService) { }
  public newCoBiForm = new FormGroup({
    material: new FormControl('', Validators.required),
    peso_bin: new FormControl('', Validators.required),
    peso_maximo: new FormControl('', Validators.required),
    // imagen: new FormControl('', Validators.required),
    ubicacion: new FormControl('', Validators.required)
  });

  ngOnInit() {
  }

  addNewCoBi(cobi: CoBi_I){
    console.log('New CoBi', cobi);
    this.postSvc.preAddAndUpdateCoBi(cobi, this.image);
  }
  handleImage(event: any): void{
    this.image = event.target.files[0];

  }

}
