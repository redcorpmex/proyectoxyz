import { CoBi_I } from './../../models/cobi.interface';
import { PostService } from './../../../components/posts/post.service';
import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import Swal from 'sweetalert2';
import { MatDialog } from '@angular/material/dialog';
import { ModalComponent } from './../modal/modal.component';


@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit, AfterViewInit {
  displayedColumns: string[] = ['id','ubicacion','imagen','material', 'peso_actual', 'peso_bin', 'peso_maximo', 'actions'];
  dataSource = new MatTableDataSource();
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true})sort: MatSort;
  constructor(private postSvc: PostService, public dialog: MatDialog) { }

  ngOnInit() {
    this.postSvc.getAllCobis().subscribe(res => {
      this.dataSource.data = res;

    })
  }
  ngAfterViewInit(){
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  onEditBin(cobi: CoBi_I){
    console.log('Edit Bin', cobi);
    this.openDialog(cobi);

  }
  onDeleteBin(cobi: CoBi_I){
    Swal.fire({
      title: 'Are you sure?',
      text: `You won't be able to revert this`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then( res => {
      if (res.value) {
        // Quiere Borrar
        this.postSvc.deleteCoBiById(cobi).then(()=>{
          Swal.fire('Deleted!', 'The CoBi has been deleted.', 'success');
        }).catch((error)=> {
          Swal.fire('Error!', 'There was an error deleting this CoBi', 'error');
        });
      }
    })
  }
  onNewCobi(){
    this.openDialog();

  }

  openDialog(cobi?: CoBi_I): void{
    const config = {
      data: {
        message: cobi ? 'Edit CoBi' : 'New CoBi',
        content: cobi
      }
    };
    const dialogRef = this.dialog.open(ModalComponent, config);
    dialogRef.afterClosed().subscribe(res => {
      console.log(`Dialog result ${res}`);
    })
  }

}
