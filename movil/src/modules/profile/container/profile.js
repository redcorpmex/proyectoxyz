import React, {Component} from 'react';
import {Alert,Image, Dimensions ,StyleSheet, AsyncStorage, TextInput} from 'react-native';
import { AppLoading } from 'expo';
import { Container, Content, Card, CardItem, Thumbnail, Text, Button, Left, Icon,Body, Form, Item, Label, Input, View} from 'native-base';
import * as Font from 'expo-font';
import { FontAwesome, Ionicons } from '@expo/vector-icons';
import io from 'socket.io-client';
import  ActivityIndicatorRN from '../components/ActivityIndicatorRN';
// import Chart from '../../charts/containers/charts'
// import Chart2 from '../../charts/containers/charts2'
import PureChart from 'react-native-pure-chart';
//import { StackedBarChart } from 'react-native-svg-charts'
import {APIurl,WebSockets} from 'react-native-dotenv'
import { element } from 'prop-types';
import { RFPercentage, RFValue } from "react-native-responsive-fontsize";
import { IconButton, Colors } from 'react-native-paper';
import Menu, { MenuItem, MenuDivider } from 'react-native-material-menu';
import AwesomeAlert from 'react-native-awesome-alerts';

const API = `${APIurl}/api/`;
const screenHeight = Math.round(Dimensions.get('window').height);
const screenWidth = Math.round(Dimensions.get('window').width);
const CardHeight = screenHeight*.3;
class Profile extends Component{
  constructor(props) {
    super(props);
    this.state = {
      Username : false,
      Ubicaciones: [
        {
          IDs: [],
          pesos: [],
          ubicacion: "",
          material_nombre:[],
          material_peso_unitario:[], 
          material_peso_maximo:[],
          bajo_10:[],
          alertEnviado:[]
        }
      ],
      Loading:false,
      porcentaje: Number,
      Historial : [],
      endpoint: `${WebSockets}`
    };
  }

  _menu = null;

  setMenuRef = ref => {
    this._menu = ref;
  };

  hideMenu = () => {
    this._menu.hide();
  };

  showMenu = () => {
    this._menu.show();
  };
  Logout = () => {
    //Hacemos una constante donde haremos toda la consulta de la consulta (redundante no?), fijate que despues del nombre del archivo va "?opcion=1" esto es importante ya que con esto podemos elegir que acción hará el archivo de consulta en PHP.
              fetch(`${API}user/auth/logout`, {
      //Le indico que la consulta es de forma POST
              method: 'GET',
      //Le indico que tipo de consulta va a obtener o su contenido
              headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json'
              },
      //Le indico que el cuerpo es un JSON y quiero que lo convierta así.
              body: JSON.stringify()
          }).then((response) => response.json()) //Si es así que realice un tipo mapeo
          .then((res) => { 
            //console.log(res);
                if(res.status === 'true') {
                  alert(res.message);
                  this.props.navigation.navigate('Login')
                }else{
                  alert(res.message);
                }
          }).done();
        }

  async userConsulta(){
    try {
      await AsyncStorage.getItem('token', async (error, resultado) => {
        await AsyncStorage.getItem('userID', async (err, result) => {
         // console.log("userConsulta -> result", result)
        if(resultado === undefined){
          this.props.navigation.navigate('Login');
        }else{
          //console.log(resultado);
          await fetch(`${API}user/${result}`, {
            //Le indico que la consulta es de forma GET
                    method: 'GET',
                    //Le indico que tipo de consulta va a obtener o su contenido
                            headers: {
                                'Accept': 'application/json',
                                'x-access-token':`${resultado}`,
                                'Content-Type': 'application/json'
                            }
      
                }).then((response) => response.json())
                .then(res => {                      
               // console.log("userConsulta -> res", res)
                
                      if(res.status === "error"  || res.status === "false") {
                        if(res.status === "error"){
                          alert(res.message);
                        }
                        this.props.navigation.navigate('Login');
                      }
                      else{
                        this.setState({ Username: res.username});
                        this.props.navigation.setParams({title: this.state.Username,
                        headerStyle:{
                        backgroundColor: '#673ab7',
                        },
                        headerTintColor: '#fff',
                        headerTitleStyle: {
                            fontWeight: 'bold',
                            opacity:54,
                        },
                        headerRight:([
                          <IconButton
                          
                          icon="bell-alert"
                          key="menu"
                          color={Colors.yellow500}
                          size={20}
                          onPress={() => console.log('Pressedooo')}
                        />,
                          <Menu
                              key="alerts"
                              ref={this.setMenuRef}
                              button={<IconButton
                                icon="dots-vertical"
                                color={Colors.white}
                                size={20}
                                onPress={() => this.showMenu()}
                              />}
                            >
                            <MenuItem onPress={this.hideMenu} disabled>
                                Perfil
                            </MenuItem>
                            <MenuDivider />
                            <MenuItem style={{alignItems:'center'}} onPress={this.Logout}>
                              <Icon ios='ios-exit' android="md-exit" style={{color: '#bf0b0b'}}/>
                          </MenuItem>
                          </Menu>
                          
                        
                      ]
                      )
                        });
                      }
                }).done();
        }
        })
      
        //console.log(result);
      });
      
    } catch (error) {
      console.log(error);
    }
  }

  async consulta() {
    this.state.Ubicaciones.map((element,index)=>{
    
      element.IDs.map(async (id,indice)=>{
        await fetch(`${API}task/bins/data/${id}`, {
          //Le indico que la consulta es de forma GET
                  method: 'GET'
                  
              }).then((response) => response.json())
              .then(async res => {
                
                    if(res._id === undefined) {
                      alert(res.message);
                    }else{
                      let dataBase = this.state.Ubicaciones;
                      
                      dataBase[index].material_nombre[indice] = res.material.nombre;
                      dataBase[index].material_peso_unitario[indice] = res.material.peso_unitario;
                      dataBase[index].material_peso_maximo[indice] = res.peso_maximo;
                      dataBase[index].alertEnviado[indice] = false;
                      this.setState({Ubicaciones: dataBase});
                      if(index === this.state.Ubicaciones.length-1){
                        if(indice === element.IDs.length-1){
                          this.setState({Loading:true})
                        }
                      }
                    }
              }).done();
        })
    })
    }


    async consultaId(posicion) {
    
      this.state.Ubicaciones[posicion].IDs.map(async (id,indice)=>{
        await fetch(`${API}task/bins/data/${id}`, {
          //Le indico que la consulta es de forma GET
                  method: 'GET'
                  
              }).then((response) => response.json())
              .then(res => {
                
                    if(res._id === undefined) {
                      
                      alert(res.message);
                    }else{
                      let dataBase = this.state.Ubicaciones;
                      dataBase[posicion].material_nombre[indice] = res.material.nombre;
                      dataBase[posicion].material_peso_unitario[indice] = res.material.peso_unitario;
                      dataBase[posicion].material_peso_maximo[indice] = res.peso_maximo;
                      dataBase[posicion].alertEnviado[indice] = false;
                      this.setState({Ubicaciones: dataBase})
                        if(indice === this.state.Ubicaciones[posicion].IDs.length-1){
                          this.setState({Loading:true})
                        }
                    }
              }).done();
        })
        
      }

      UNSAFE_componentWillMount(){
        this.userConsulta();
      }


  async componentDidMount(){
   

    const { endpoint } = this.state;
    this.setState({porcentaje:100});
    this.socket = io.connect(endpoint,connectionConfig);
    this.socket.on("newUbi",data=>{
      console.log(this.state.Ubicaciones);
      this.setState({Loading:false})
      this.setState({Ubicaciones: data.Ubicaciones},async ()=>{
        await this.consulta();
      })
    })
    this.socket.on("newID",data=>{
      this.setState({Loading:false})
      let theIds = this.state.Ubicaciones;
      console.log("theIds ", theIds )
      theIds[data.posicion].IDs = data.IDs;
      theIds[data.posicion].pesos = data.pesos;
      theIds[data.posicion].ubicacion = data.ubicacion;
      this.setState({Ubicaciones: theIds},()=>{
        this.consultaId(data.posicion);
      });
    });
    if(this.state.Ubicaciones.length>0){
      this.socket.on("Peso", data=> {
          let theWeights = this.state.Ubicaciones;
          theWeights[data.posicion].pesos =data.pesos;
          this.setState({Ubicaciones: theWeights});
        })
    }
    
      
  }
  
  loadChart(){
    
    
    return this.state.Ubicaciones.map((element,index)=>{
      let builtData = [];
      let finalData = [];
      let menorA20;
      let color;

      if(element.material_nombre.length === element.IDs.length){
        element.IDs.map((elemento,indice)=>{
          //console.log(this.state.Ubicaciones[index].bajo_10[indice]);  
          if(this.state.Ubicaciones[index].material_nombre[indice] !== undefined){ 
            if((this.state.Ubicaciones[index].pesos[indice]/
            this.state.Ubicaciones[index].material_peso_maximo[indice]*100)<20 ){
                  menorA20=true;
                  color = 'yellow';
              if((this.state.Ubicaciones[index].pesos[indice]/
                this.state.Ubicaciones[index].material_peso_maximo[indice]*100)<10 ){
            
              color = 'red';
                }               
            }
            else{
              menorA20 = false;
              color = '#05cc47';
            }
            
            if(this.state.Ubicaciones[index].alertEnviado[indice]){
              
              this.state.Ubicaciones[index].bajo_10[indice]=false;
              if((this.state.Ubicaciones[index].pesos[indice]/
                this.state.Ubicaciones[index].material_peso_maximo[indice]*100)>20){
                this.state.Ubicaciones[index].alertEnviado[indice]=false;
                this.state.Ubicaciones[index].bajo_10[indice]=false;
              }
            }
            else{
              
              if(menorA20){

                this.state.Ubicaciones[index].bajo_10[indice] = true;
              }
              else{
                this.state.Ubicaciones[index].bajo_10[indice] = false;
              }
            }
            if(this.state.Ubicaciones[index].bajo_10[indice]){
              Alert.alert('Alerta',
              `Hay menos del 20% de ${this.state.Ubicaciones[index].material_nombre[indice]} en el ${element.ubicacion}`,
              [
                {text: 'OK', onPress: () => {console.log('OK Pressed')}},
              ],
            {cancelable: false},);
            this.state.Ubicaciones[index].alertEnviado[indice] = true;
            } 
            builtData.push({x: this.state.Ubicaciones[index].material_nombre[indice], 
              y: this.state.Ubicaciones[index].pesos[indice]/this.state.Ubicaciones[index].material_peso_maximo[indice]*100}
            ) 
              
          }
          //finalData.push({data:builtData, color:color})
          //finalData.push({data:[builtData[indice]], color:color})
          //console.log("loadChart -> finalData", finalData)
          
        })
        finalData.push({data:builtData, color:'#05cc47'})
        
          
        {/*Se puede arreglo de colors???*/}
          let showAlert=true;
        return([
          <Card style={styles.card} key ={element.ubicacion}>
            <View>
            <Text style={{fontFamily:'Roboto',fontSize: RFPercentage(2.5)}} >
              {element.ubicacion}
            </Text>
            <PureChart  data={finalData}  type='bar'/>
            </View>
            
          </Card>
        ]
        )
      }
      
    })
  }

    render() {
    return (
        <Container >
          {this.state.Loading
            ? <Container style={styles.page}>
                {this.loadChart()}
            </Container>
            : <View style={[styles.container, styles.horizontal]}><ActivityIndicatorRN/></View> }
        </Container>
      );
    }
  }

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    page: {
      flex: 1,
      backgroundColor:'#f0e9f5',
      alignItems: 'center',
      justifyContent: 'center',
    },
    card:{
      position:'relative',
      shadowColor: "#6F08B4",
            shadowOffset: {
	            width: 0,
	            height: 9,
            },
            opacity:0.95,
            shadowOpacity: 0.50,
             shadowRadius: 12.35,

            elevation: 19,
            alignItems:'center',
            width:'70%',
            height:'30%'
    },
      horizontal: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        padding: 10
      }
  });
  const connectionConfig = {
    timeout: 10000,
    jsonp: false,
    transports:['websocket'],
     //autoConnect:false,
    agent: '-',
     path: '/socket.io/?EIO=3', // Whatever your path is
    pfx: '-',
     key: '', // Using token-based auth.
     passphrase: '', // Using cookie auth.
    cert: '-',
    ca: '-',
    ciphers: '-',
    rejectUnauthorized: '-',
    perMessageDeflate: '-'
  };
export default Profile