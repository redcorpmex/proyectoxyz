import { finalize } from 'rxjs/operators';
import { File_I } from './../models/file.interface';
import { Injectable } from '@angular/core';
import { User_I } from  '../models/user.interface';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireStorage } from '@angular/fire/storage';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  public userData$: Observable<firebase.User>;
  private filepath: string;

  constructor(private afAuth: AngularFireAuth, private storage: AngularFireStorage) {
    this.userData$ = afAuth.authState;
  }

    loginByEmail(user: User_I) {
      const {email, password} = user;
      return this.afAuth.auth.signInWithEmailAndPassword(email, password);
    }

    logout(){
      this.afAuth.auth.signOut();
    }

    preSaveUserProfile(user: User_I, image?:File_I ): void {
      if (image) {
        this.uploadImage(user, image);
      }else{
        this.saveUserProfile(user);
      }
    }

    private uploadImage(user: User_I, image: File_I): void {
      this.filepath = `images/${image.name}`;
      const fileRef = this.storage.ref(this.filepath);
      const task = this.storage.upload(this.filepath, image);
      task.snapshotChanges().pipe(
        finalize(() => {
          fileRef.getDownloadURL().subscribe(urlImage => {
            user.photoURL = urlImage;
            this.saveUserProfile(user);
          });
        })
      ).subscribe();
    }

    private saveUserProfile(user: User_I) {
      this.afAuth.auth.currentUser.updateProfile({
        displayName: user.displayName,
        photoURL: user.photoURL
      })
      .then(()=> {
        console.log('User Updated!');
        location.reload();
      })
      .catch(err => {
        console.log('Error', err);
      })

    }
}
