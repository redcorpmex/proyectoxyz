export interface CoBi_I{
    id?: string,
    material: string;
    peso_bin: number;
    peso_maximo: number;
    peso_actual?: number;
    ubicacion: string;
    imagen?: any;
    fileRef?: string;
}
