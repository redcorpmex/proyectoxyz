import { Observable } from 'rxjs';
import { PostService } from './../post.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CoBi_I } from 'src/app/shared/models/cobi.interface';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {

  // public cobi: {
  //   id: string;
  //   material: string;
  //   peso_bin: number;
  //   peso_maximo: number;
  //   peso_actual: number;
  // }

public cobi$ : Observable<CoBi_I>;
  constructor(private route: ActivatedRoute, private postSvc: PostService) { }

  ngOnInit() {
    const idCobi = this.route.snapshot.params.id;
    this.cobi$ = this.postSvc.getOneCobi(idCobi);
  }

}
