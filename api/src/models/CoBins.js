const {Schema ,model} = require('mongoose');

const BinsSchema =  new Schema({
    material: {
            type: Schema.Types.ObjectId,
            ref: 'Material'
    },
    peso_bin: Number,
    peso_maximo: Number,
    peso_actual: Number,
    created: { 
        type: Date,
        default: Date.now
    }
});



module.exports = model('Bin', BinsSchema);
