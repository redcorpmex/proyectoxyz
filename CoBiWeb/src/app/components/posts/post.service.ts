import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map, finalize } from 'rxjs/operators';
import { CoBi_I } from '../../shared/models/cobi.interface';
import { File_I } from '../../shared/models/file.interface';
import { AngularFireStorage } from '@angular/fire/storage';
//import { Action } from 'rxjs/internal/scheduler/Action';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  private cobiCollection: AngularFirestoreCollection<CoBi_I>;
  private filePath: any;
  private downloadURL: Observable<string>;

  constructor(private afs: AngularFirestore, private storage: AngularFireStorage) {
    this.cobiCollection = afs.collection<CoBi_I>('cobi');
   }

  public getAllCobis():Observable<CoBi_I[]>{
    return this.cobiCollection
    .snapshotChanges()
    .pipe(
      map(actions =>
        actions.map(a => {
        const data = a.payload.doc.data() as CoBi_I;
        const id = a.payload.doc.id;
        return {id, ...data};
      })
     )
    );
  }

  public getOneCobi(id: CoBi_I): Observable<CoBi_I> {
    return this.afs.doc<CoBi_I>(`cobi/${id}`).valueChanges();
  }

  public deleteCoBiById(cobi: CoBi_I){
    return this.cobiCollection.doc(cobi.id).delete();
  }

  public editCoBiById(cobi: CoBi_I, newImage?: File_I){
    if (newImage) {
      this.uploadImage(cobi, newImage);
    }else{
      return this.cobiCollection.doc(cobi.id).update(cobi);
    }
  }

  public preAddAndUpdateCoBi(cobi: CoBi_I, image: File_I): void{
    this.uploadImage(cobi, image);
  }

  private uploadImage(cobi: CoBi_I, image: File_I) {
    this.filePath = `images/${image.name}`;
    const fileRef = this.storage.ref(this.filePath);
    const task = this.storage.upload(this.filePath, image);
    task.snapshotChanges().pipe(
      finalize(() => {
        fileRef.getDownloadURL().subscribe( urlImage => {
          this.downloadURL = urlImage;
          this.saveCoBi(cobi);
          // Call AddCoBi()
        })
      })
    ).subscribe();
  }

  private saveCoBi(cobi: CoBi_I){

    const cobiObj = {
      material: cobi.material,
      peso_bin: cobi.peso_bin,
      peso_maximo: cobi.peso_maximo,
      ubicacion: cobi.ubicacion,
      imagen: this.downloadURL,
      fileRef: this.filePath
    }
    if (cobi.id) {
      return this.cobiCollection.doc(cobi.id).update(cobiObj);
    }else {
      return this.cobiCollection.add(cobiObj);
    }
  }
}
