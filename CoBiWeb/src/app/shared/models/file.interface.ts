export interface File_I{
  name: string;
  imageFile: File;
  size: string;
  type: string;
}
