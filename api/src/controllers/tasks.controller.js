const passport = require('passport');
const admin = require('firebase-admin');
//const serviceAccountKey = require("../../serviceAccountKey");
import serviceAccountKey from '../../serviceAccountKey';
require('../databasemongo');
const CoBin = require('../models/CoBins');
const Material = require('../models/Material');
console.log(serviceAccountKey);
admin.initializeApp({
    credential: admin.credential.cert(serviceAccountKey),
    databaseURL: "https://cobinet-58d35.firebaseio.com"
});

const db = admin.firestore();

const getAllData = async (req, res) => {
    try {
        const response = await CoBin.find().populate('material');
        console.log(response);
        return res.status(200).json(response);
    } catch (e) {
        res.status(500).json({
            status: 'error',
            message: 'Something goes wrong',
            data: {}
        });
    }
};


const getBinsData = async (req, res) => {
    try {
        const response = await CoBin.findById(req.params.id).populate('material');
        console.log(response);
        return res.status(200).json(response);
    } catch (e) {
        res.status(500).json({
            status: 'error',
            message: 'Something goes wrong',
            data: {}
        });
    }
};

const getMaterialData = async (req, res) => {
    try {
        const response = await Material.find();
        console.log(response);
        return res.status(200).json(response);
    } catch (e) {
        res.status(500).json({
            status: 'error',
            message: 'Something goes wrong',
            data: {}
        });
    }
};

const createBinsData = async (req, res) => {
    try {
        const {material, peso_bin, peso_maximo} = req.body;
        const newData = new CoBin({material, peso_bin, peso_maximo});
        await newData.save();
        res.status(200).json({
            status: 'true',
            message: 'Bin Created',
            data: {}
        });
    } catch (e) {
        res.status(500).json({
            status: 'error',
            message: 'Something goes wrong',
            data: {}
        });
    }
};
//Opción para crear Bin con material:
/* user = req.params;
        id = user.id;
        const { title, subtitle} = req.body;
        const post = await Post.create({
            title,
            subtitle,
            user:id
        });
        const userById = await User.findById(id);

        userById.posts.push(post);
        await userById.save();

        return res.send(userById);
    },*/

const createMaterialData = async (req, res) => {
    try {
        const {nombre,peso_unitario} = req.body;
        const newData = new Material({nombre,peso_unitario});
        await newData.save();
        res.status(200).json({
            status: 'true',
            message: 'Material Created',
            data: {}
        });
    } catch (e) {
        res.status(500).json({
            status: 'error',
            message: 'Something goes wrong',
            data: {}
        });
    }
};
const deleteBinsData = async (req, res) => {
    try {
        
        await CoBin.findByIdAndDelete(req.params.id);
        res.status(200).json({
            status: 'true',
            message: 'Bin Deleted',
            data: {}
        });
    } catch (e) {
        res.status(500).json({
            status: 'error',
            message: 'Something goes wrong',
            data: {}
        });
    }
};

const deleteMaterialData = async (req, res) => {
    try {
        await Material.findByIdAndDelete(req.params.id);
        res.status(200).json({
            status: 'true',
            message: 'Bin Deleted',
            data: {}
        });
    } catch (e) {
        res.status(500).json({
            status: 'error',
            message: 'Something goes wrong',
            data: {}
        });
    }
};

const updateData = async (req, res) => {
    try {
        const {peso_actual} = req.body;
        await CoBin.findByIdAndUpdate(req.params.id,{peso_actual});
        let cityRef = db.collection('cobi').doc(req.params.id);
        await cityRef.get()
        .then(doc => {
        if (!doc.exists) {
            console.log('No such document!');
        } else {
            cityRef.update({
                peso_actual
            });
        }
        })
        .catch(err => {
            console.log('Error getting document', err);
       });
        //console.log(db.collection('cobi').doc(req.params.id).get());
        
          /*await db.collection('cobi').doc(req.params.id).update({
          peso_actual})*/
        
        //console.log(req.body);
        //await CoBin.save();
        res.status(200).json({
            status: 'true',
            message: 'Bin Updated',
            data: {}
            
        });
       // return admin.database().ref().update(update);
    } catch (e) {
        res.status(500).json({
            status: 'error',
            message: 'Something goes wrong',
            data: {}
        });
    }
};
const updateBinsData = async (req, res) => {
    try {
        const {material, peso_bin, peso_maximo} = req.body;
        await CoBin.findByIdAndUpdate(req.params.id,{material, peso_bin, peso_maximo});
        //await CoBin.save();
        res.status(200).json({
            status: 'true',
            message: 'Bin Updated',
            data: {}
        });
        
    } catch (e) {
        res.status(500).json({
            status: 'error',
            message: 'Something goes wrong',
            data: {}
        });
    }
};

const updateMaterialData = async (req, res) => {
    try {
        const {nombre, peso_unitario} = req.body;
        await Material.findByIdAndUpdate(req.params.id,{nombre, peso_unitario});
        //await CoBin.save();
        res.status(200).json({
            status: 'true',
            message: 'Material Updated',
            data: {}
        });
        
    } catch (e) {
        res.status(500).json({
            status: 'error',
            message: 'Something goes wrong',
            data: {}
        });
    }
};

module.exports = {
    getAllData,
    getBinsData,
    getMaterialData,
    createBinsData,
    createMaterialData,
    deleteBinsData,
    deleteMaterialData,
    updateData,
    updateBinsData,
    updateMaterialData
}