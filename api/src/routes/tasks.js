const { Router } = require('express');
const router = Router();
const { isLoggedIn, isNotLoggedIn } = require('../lib/auth');
const verifyToken = require('../lib/verifyToken');
const { getAllData, getBinsData,getMaterialData, createBinsData,createMaterialData,deleteBinsData,deleteMaterialData,updateData, updateBinsData, updateMaterialData } = require('../controllers/tasks.controller');

router.get('/',getAllData);
router.get('/bins/data/:id', getBinsData);
router.get('/material', getMaterialData);
router.post('/bins/data/',createBinsData);
router.post('/material/data/', createMaterialData);
router.post('/bins/data/update/:id', updateData);
router.post('/bins/data/updateBin/:id', updateBinsData);
router.post('/material/data/update/:id', updateMaterialData);
router.delete('/bins/data/:id',deleteBinsData);
router.delete('/material/data/:id',deleteMaterialData);
// router.put('/bins/data/:id',isLoggedIn, verifyToken, updateBinsData);

module.exports = router;