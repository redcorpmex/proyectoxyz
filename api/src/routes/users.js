const { Router } = require('express');
const router = Router();
const { isLoggedIn, isNotLoggedIn } = require('../lib/auth');
const verifyToken = require('../lib/verifyToken');
const { getUsers, createUser, getUserById, getUserByIdAdmin, deleteUser, updateUser, Success, Failure, logOut } = require('../controllers/users.controller');

router.get('/', isLoggedIn, verifyToken,getUsers);
router.get('/:id', isLoggedIn, verifyToken, getUserByIdAdmin);
router.post('/auth/signup', isNotLoggedIn, createUser);
router.post('/auth/signin', isNotLoggedIn, getUserById);
router.delete('/:id', isLoggedIn, verifyToken, deleteUser);
router.put('/:id', isLoggedIn, verifyToken,updateUser);
router.get('/auth/successjson', isLoggedIn ,Success);
router.get('/auth/failurejson', Failure );
router.get('/auth/logout',isLoggedIn, logOut);

module.exports = router;