import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CoBi_I } from 'src/app/shared/models/cobi.interface';
import { PostService } from './../post.service';


@Component({
  selector: 'app-edit-post',
  templateUrl: './edit-post.component.html',
  styleUrls: ['./edit-post.component.scss']
})
export class EditPostComponent implements OnInit {
  private image: any;
  private imageOriginal: any;

  @Input() cobi: CoBi_I;

  constructor(private postSvc: PostService) { }

  public editCoBiForm= new FormGroup({
    id: new FormControl('', Validators.required),
    material: new FormControl('', Validators.required),
    peso_bin: new FormControl('', Validators.required),
    peso_maximo: new FormControl('', Validators.required),
    // imagen: new FormControl('', Validators.required),
    ubicacion: new FormControl('', Validators.required),
  });

  ngOnInit() {
    this.image = this.cobi.imagen;
    this.imageOriginal = this.cobi.imagen;
    this.initValuesForm();
  }

  editCoBi(cobi: CoBi_I){
    if (this.image === this.imageOriginal) {
      cobi.imagen = this.imageOriginal;
      // Call method(cobi)
      this.postSvc.editCoBiById(cobi);
    }else {
      // Call method(cobi, this.image);
      this.postSvc.editCoBiById(cobi, this.image);
    }

  }

  handleImage(event: any): void{
    this.image = event.target.files[0];
  }

  private initValuesForm(): void {
    this.editCoBiForm.patchValue({
      id: this.cobi.id,
      material: this.cobi.material,
      peso_bin: this.cobi.peso_bin,
      peso_maximo: this.cobi.peso_maximo,
      ubicacion: this.cobi.ubicacion
    });
  }

}
