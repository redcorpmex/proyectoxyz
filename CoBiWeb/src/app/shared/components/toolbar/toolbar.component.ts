import { Router } from '@angular/router';
import { AuthService } from './../../service/auth.service';
import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {
  public appname = 'CoBiNet'
  public opened = false;
  constructor(public authSvc: AuthService, private router: Router) { }

  ngOnInit() {
  }

  onLogout(): void{
    Swal.fire({
      title: 'Are you sure?',
      text: `unsaved changes will be lost`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Exit!'
    }).then( res => {
      if (res.value) {
        Swal.fire('Bye!', 'You left successfully!!', 'success');
        this.router.navigate(['/']);
        this.authSvc.logout();
        }
    })
  }

  onEnterAdmin(){
    Swal.fire({
      title: 'Are you sure?',
      text: `Any changes you make will be irreversible`,
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, Enter!'
    }).then( res => {
      if (res.value) {
        Swal.fire('Access Granted!', 'Access has been verified!!', 'success');
        this.router.navigate(['/admin/cobis']);
        }
    })
  }
}
