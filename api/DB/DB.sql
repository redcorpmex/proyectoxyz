	create database if not exists cobiusers;
	use cobiusers;

create table if not exists usuario (
	id_usuario 	int(11)		auto_increment,
    	nombre		varchar(40) not null,
	username		varchar(15) not null,
    	apellidos	varchar(50)	not null,
    	telefono	varchar(10) null,
	password	varchar(60) 	not null,
	email	varchar(40)		not null,
    	created_at timestamp NOT NULL DEFAULT current_timestamp,
    	primary key(id_usuario) )
    		Engine= InnoDB 
    		character set latin1 
    		auto_increment = 1; 

CREATE TABLE IF NOT EXISTS `sessions` (
  session_id varchar(128) COLLATE utf8mb4_bin NOT NULL,
  expires int(11) unsigned NOT NULL,
  data text COLLATE utf8mb4_bin,
  PRIMARY KEY (session_id)
) ENGINE=InnoDB;

