import React from 'react'
import { Container, Content, Card, CardItem, Thumbnail, Text, Button, Left,Body, Form, Item, Label, Input, View} from 'native-base';
import PureChart from 'react-native-pure-chart';

//default data is available 
let sampleData = [
    {
      seriesName: 'series1',
      data: [
        {x: '2018-02-01', y: 30},
        {x: '2018-02-02', y: 200},
        {x: '2018-02-03', y: 170},
        {x: '2018-02-04', y: 250},
        {x: '2018-02-05', y: 10}
      ],
      color: '#297AB1'
    },
    {
      seriesName: 'series2',
      data: [
        {x: '2018-02-01', y: 20},
        {x: '2018-02-02', y: 100},
        {x: '2018-02-03', y: 140},
        {x: '2018-02-04', y: 550},
        {x: '2018-02-05', y: 40}
      ],
      color: 'yellow'
    }
  ]
//default data is available 

class Charts2 extends React.PureComponent {
    render() {
        return (
            
            <PureChart data={sampleData} type='bar' />
           );
    }
}

export default Charts2