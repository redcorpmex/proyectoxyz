const pool = require('../database');
const passport = require('passport');

const getUsers = async (req, res) => {
    try {
        const response = await pool.query('SELECT * FROM usuario');
        console.log(response);
        return res.status(200).json(response);
    } catch (e) {
        res.status(500).json({
            status: 'error',
            message: 'Something goes wrong',
            data: {}
        });
    }
};

const getUserById =  (req, res, next) => {
    passport.authenticate('local.signin', {
        successRedirect: '/api/user/auth/successjson',
        failureRedirect: '/api/user/auth/failurejson',
        failureFlash: false
    })(req, res, next);
};

const getUserByIdAdmin =  async (req, res) => {
    try {
        const response = await pool.query('SELECT * FROM usuario WHERE id_usuario = ?', [req.params.id]);
        response[0].message =['https://s3.amazonaws.com/uifaces/faces/twitter/josephstein/128.jpg'];
        response[0].status ='true';
        console.log(response);
        return res.json(response[0]);
    } catch (e) {
        res.status(500).json({
            status: 'error',
            message: 'Something goes wrong',
            data: {}
        });
    }
};

const createUser = passport.authenticate('local.signup', {
    successRedirect: '/api/user/auth/successjson',
    failureRedirect: '/api/user/auth/failurejson',
    failureFlash: true,
    session: true
});


const deleteUser = async (req, res) => {
    try {
        const response = await pool.query('DELETE FROM usuario WHERE id_usuario = ?', [req.params.id]);
        console.log(response);
        return res.json({
            status: 'true',
            message: 'User Deleted Succesfully'
        });
    } catch (e) {
        res.status(500).json({
            status: 'error',
            message: 'Something goes wrong',
            data: {}
        });
    }
};

const updateUser = async (req, res) => {
    try {
        const { nombre, username, apellidos,telefono,email  } = req.body;
        const response = await pool.query('UPDATE usuario SET nombre = ?, username = ?,apellidos = ?,telefono = ?,email = ? WHERE id_usuario = ?', [nombre, username, apellidos, telefono, email, req.params.id]);
        console.log(response); // Necesita adaptarse a MySQL
        return res.json({
            status: 'true',
            message: 'User Updated Succesfully',
        });
    } catch (e) {
        res.status(500).json({
            status: 'error',
            message: 'Something goes wrong',
            data: {}
        });
    }
};

const Success = (req, res) => {
    console.log(req.user);
    res.json(req.user);
};
const Failure = async (req, res) => {
     await res.json({
        status: 'false', 
        message: 'Start Session Failure'});
};

const logOut =  (req, res) => {
    req.logOut();
    // console.log(req.session.passport.user); 
    // delete req._passport.session.user;
    // console.log(req.session.passport.user);
    res.clearCookie("session");
    res.clearCookie("session.sess");
    return res.json({
        status: 'true',
        message: 'Session closed successfully'
    });
  };

module.exports = {
    getUsers,
    createUser,
    getUserById,
    getUserByIdAdmin,
    deleteUser,
    updateUser,
    Success,
    Failure,
    logOut
}